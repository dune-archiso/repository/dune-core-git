# [`dune-core-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune-core-git) metapackage

| `README.md`                                                                                                                        | Latest build from [GitHub](https://github.com/dune-project)'s package (DD/MM/YY) | `pkg.tar.zst` package                                                                                                                                                                               |
| :--------------------------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------------------: | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [`dune-common-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-common-git/README.md)                 |                                    23/04/2022                                    | [`dune-common-git-2.10.r11636.534337714-1-x86_64.pkg.tar.zst`](https://dune-archiso.gitlab.io/repository/dune-core-git/x86_64/dune-common-git-2.10.r11636.534337714-1-x86_64.pkg.tar.zst)             |
| [`dune-geometry-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-geometry-git/README.md)             |                                    23/04/2022                                    | [`dune-geometry-git-2.10.r1203.81122d3-1-x86_64.pkg.tar.zst`](https://dune-archiso.gitlab.io/repository/dune-core-git/x86_64/dune-geometry-git-2.10.r1203.81122d3-1-x86_64.pkg.tar.zst)               |
| [`dune-grid-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-grid-git/README.md)                     |                                    23/04/2022                                    | [`dune-grid-git-2.10.r11494.5ee8784d1-1-x86_64.pkg.tar.zst`](https://dune-archiso.gitlab.io/repository/dune-core-git/x86_64/dune-grid-git-2.10.r11494.5ee8784d1-1-x86_64.pkg.tar.zst)                 |
| [`dune-istl-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-istl-git/README.md)                     |                                    23/04/2022                                    | [`dune-istl-git-2.10.r3476.59188482-1-x86_64.pkg.tar.zst`](https://dune-archiso.gitlab.io/repository/dune-core-git/x86_64/dune-istl-git-2.10.r3476.59188482-1-x86_64.pkg.tar.zst)                     |
| [`dune-localfunctions-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-localfunctions-git/README.md) |                                    23/04/2022                                    | [`dune-localfunctions-git-2.10.r1850.8ad06c5b-1-x86_64.pkg.tar.zst`](https://dune-archiso.gitlab.io/repository/dune-core-git/x86_64/dune-localfunctions-git-2.10.r1850.8ad06c5b-1-x86_64.pkg.tar.zst) |

### Tests

- `dune-common-git` OK.
- `python-dune-common-git` OK.
- `dune-geometry-git` OK.
- `python-dune-geometry-git` OK.
- `dune-istl-git` OK.
- `python-dune-istl-git` OK.
- `dune-localfunctions-git` OK.
- `python-dune-localfunctions-git` OK.
- `dune-grid-git` OK.
- `python-dune-grid-git` OK.
